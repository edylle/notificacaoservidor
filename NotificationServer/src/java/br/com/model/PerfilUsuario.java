package br.com.model;

public enum PerfilUsuario {

    PUBLICO, SEGURANCA, ADMIN
}
