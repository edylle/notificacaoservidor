package br.com.controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.dao.ClientDeviceDAO;
import br.com.dao.UsuarioDAO;
import br.com.model.Usuario;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

@ManagedBean
@Path("/GCM")
public class RestGCMNotification {

    public static final String GOOGLE_GCM_HTTP = "https://android.googleapis.com/gcm/send";
    public static final String GOOGLE_API_KEY = "AIzaSyD1ysOAYSYHXrARgkb2xb13pPPUzDBNiyo";
    public static final String MESSAGE_KEY = "mensagem";

    @GET
    @Path("/saveregistration/{regId}")
    @Produces(MediaType.TEXT_PLAIN)
    public String saveRegId(@PathParam("regId") String regId) {
        Usuario usuario = new Usuario(regId);
        UsuarioDAO dao = new UsuarioDAO();
        if (!regIdIsNew(regId)) {
            return "SUCCESS1";
        }
        dao.startOperation();
        dao.save(usuario);
        dao.stopOperation(true);
        return "SUCCESS";
    }

    @GET
    @Path("/sendnotificationbroadcast/{mensagem}")
    public void send(@PathParam("mensagem") String mensagem) {
        String messageToSend = mensagem;

        System.out.println("MENSAGEM: " + messageToSend);

        UsuarioDAO dao = new UsuarioDAO();
        dao.startOperation();
        List<Usuario> devices = dao.findAll(Usuario.class);
        dao.stopOperation(false);

        List<String> regIds = new ArrayList<String>();
        for (Usuario usuario : devices) {

            System.out.println("ENCONTRADO NO BD: " + usuario.getRegId());

            regIds.add(usuario.getRegId());
        }

        System.out.println("RegIDs: " + regIds.size());
        MulticastResult results = null;
        Sender sender = new Sender(GOOGLE_API_KEY);
        Message.Builder mBuilder = new Message.Builder();
        mBuilder.timeToLive(30); // Matém até 10 segundos
        mBuilder.addData(MESSAGE_KEY, messageToSend);
        Message message = mBuilder.build();
        try {
            results = sender.send(message, regIds, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Result> listaResults = results.getResults();
        int i = 0;
        for (Result result : listaResults) {
            if (!(result.getErrorCodeName() == null)) {
                System.out.println("RESULTADO: " + result.getErrorCodeName());
            }
            if (!(result.getCanonicalRegistrationId() == null)) {
                System.out.println("ID CANONICAL: "
                        + result.getCanonicalRegistrationId());
                dao.startOperation();
                dao.delete(devices.get(i));
                dao.stopOperation(true);
            }
            i++;
        }

    }

    @GET
    @Path("/teste/{regId}")
    @Produces(MediaType.TEXT_PLAIN)
    public void teste(@PathParam("regId") String regId) {
        System.out.println(regId);
    }

    private boolean regIdIsNew(String regId) {
        ClientDeviceDAO dao = new ClientDeviceDAO();
        dao.startOperation();
        if (dao.buscarPeloRegId(regId).size() == 0) {
            dao.stopOperation(false);
            return true;
        }
        dao.stopOperation(false);
        return false;
    }

}
