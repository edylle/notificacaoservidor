package br.com.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.model.ClientDevice;

public class ClientDeviceDAO extends BaseDAO<ClientDevice> {

    @SuppressWarnings("unchecked")
    public List<Object> buscarPeloRegId(String regId) {
        String hql = "SELECT device FROM " + ClientDevice.class.getName() + " AS device WHERE device.regId = :regId";
        System.out.println("HQL: " + hql);
        Query query = getEntityManager().createQuery(hql);
        query.setParameter("regId", regId);
        System.out.println("RESULTADO DA CONSULTA: " + query.getResultList().size());
        return query.getResultList();
    }

}
